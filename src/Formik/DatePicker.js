import React from "react";
import DatePicker from "react-datepicker";
import styled from "styled-components";

import { Label } from "./Label";
import { Error } from "./Error";

const ReactDatePicker = ({
  disabled,
  className,
  touched,
  errors,
  name,
  label,
  ...rest
}) => {
  const error = touched[name] && errors[name];
  console.log("DATE ERROR", error, errors);
  return (
    <StyledWrapper className={className}>
      {label && <Label htmlFor={id} error={error} label={label} />}
      <StyledDate disabled={disabled} className={className} error={error}>
        <DatePicker
          disabled={disabled}
          name={name}
          popoverAtachment="bottom left"
          popoverTargetAttachment="top left"
          {...rest}
        />
      </StyledDate>
      {error && <Error error={error} />}
    </StyledWrapper>
  );
};

export default ReactDatePicker;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: 500;
  font-size: 14px;
  width: 100%;
  margin-bottom: 5px;
  position: relative;
`;

const StyledDate = styled.div`
  .react-datepicker__input-container {
    display: flex;

    > input {
      text-align: left;
      padding-left: 0.7rem;
      background: ${p => (p.disabled ? p.theme.color.bg : "#ffffff")};
      border: ${p =>
        p.error
          ? `1px solid ${p.theme.color.danger} !important`
          : `1px solid #96c0e4 !important`};

      &:focus {
        border-color: ${p => p.theme.color.primary} !important;
      }
    }
  }
`;
