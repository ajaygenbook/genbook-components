import React from "react";
import Select from "react-select";
import styled from "styled-components";
import { Error } from "./Error";
import { Label } from "./Label";

class Selecty extends React.Component {
  handleChange(value) {
    this.props.onChange(this.props.name, value);
  };

  render() {
    const {
      id,
      label,
      name,
      options,
      isMulti,
      value,
      errors,
      touched,
      className,
      ...rest
    } = this.props;

    const customStyles = {
      control: base => ({
        ...base,
        minHeight: 34
      }),
      dropdownIndicator: base => ({
        ...base,
        padding: 4
      }),
      clearIndicator: base => ({
        ...base,
        padding: 4
      }),
      multiValue: base => ({
        ...base
      }),
      valueContainer: base => ({
        ...base,
        padding: "0px 6px"
      }),
      input: base => ({
        ...base,
        margin: 0,
        padding: 0
      })
    };
    const error = touched[name] && errors[name];
    return (
      <StyledWrapper className={className}>
        <Label htmlFor={id} label={label} />
        <StyledSelect
          id={id}
          name={name}
          options={options}
          isMulti={isMulti}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={value}
          styles={customStyles}
          error={error}
          {...rest}
        />
        {!!error && <Error error={error} />}
      </StyledWrapper>
    );
  }
}

export default Selecty;

const StyledSelect = styled(Select)`
  > div {
    border-color: ${p => (p.error ? p.theme.color.danger : "#96c0e4")};
    border-radius: 2px;

    &:focus,
    &:hover {
      border: 1px solid ${p => p.theme.color.primary};
    }
    & [class*="indicatorContainer"] {
      padding: 5px;
    }
  }
`;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: 500;
  font-size: 14px;
  width: 100%;
  margin-bottom: 5px;
  position: relative;
`;
