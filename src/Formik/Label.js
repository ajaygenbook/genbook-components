import React from "react";
import styled from "styled-components";

export const Label = ({ label, error, ...rest }) => (
  <StyledLabel error={error} {...rest}>
    {label}
  </StyledLabel>
);

const StyledLabel = styled.label`
  font-size: 13px;
  font-weight: 500;
  margin: 0 0 0.285714rem;
  text-transform: none;
  touch-action: manipulation;
  color: ${p => p.theme.color.text};
  text-align: left;
`;
