import React from "react";
import styled from "styled-components";
import AsyncSelect from "react-select/async";

const AsyncSelecty = props => {
  const customStyles = {
    control: base => ({
      ...base,
      minHeight: 34
    }),
    dropdownIndicator: base => ({
      ...base,
      padding: 4
    }),
    clearIndicator: base => ({
      ...base,
      padding: 4
    }),
    multiValue: base => ({
      ...base
    }),
    valueContainer: base => ({
      ...base,
      padding: "0px 6px"
    }),
    input: base => ({
      ...base,
      margin: 0,
      padding: 0
    })
  };

  return <StyledAsyncSelect styles={customStyles} {...props} />;
};

export default AsyncSelecty;

const StyledAsyncSelect = styled(AsyncSelect)`
  > div {
    border-color: ${p => (p.error ? p.theme.color.danger : "#96c0e4")};
    border-radius: 2px;

    &:focus,
    &:hover {
      border: 1px solid ${p => p.theme.color.primary};
    }
    & [class*="indicatorContainer"] {
      padding: 5px;
    }
  }
`;
