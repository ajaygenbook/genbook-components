import React from "react";
import styled from "styled-components";

export const Error = ({ error, className, ...rest }) => (
  <StyledError className={className}>{error}</StyledError>
);

const StyledError = styled.span`
  color: ${p => p.theme.color.danger};
  margin-bottom: 5px;
`;
