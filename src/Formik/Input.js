import React from "react";
import { Field } from "formik";
import styled from "styled-components";

import { Label } from "./Label";
import { Error } from "./Error";

export const FormikInput = ({ label, id, name, className, type, ...rest }) => {
  return (
    <Field
      component={InputComponent}
      label={label}
      id={id}
      name={name}
      className={className}
      type={type}
      {...rest}
    />
  );
};

const InputComponent = ({
  field: { name, value, onChange, onBlur },
  form: { errors, touched },
  id,
  label,
  customValue,
  className,
  type,
  ...rest
}) => {
  const error = touched[name] && errors[name];
  return (
    <StyledWrapper className={className}>
      {label && <Label htmlFor={id} error={error} label={label} />}
      <StyledInput
        name={name}
        id={id}
        type={type || "text"}
        value={customValue || value}
        onChange={onChange}
        onBlur={onBlur}
        error={error}
        {...rest}
      />
      {error && <Error error={error} />}
    </StyledWrapper>
  );
};

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: 500;
  font-size: 14px;
  width: 100%;
  margin-bottom: 5px;
  position: relative;
`;

const StyledInput = styled(Field)`
  border-radius: 2px !important;
  border: ${p =>
    p.error
      ? `1px solid ${p.theme.color.danger} !important`
      : `1px solid #96c0e4 !important`};
  background-color: ${p => (p.disabled ? p.theme.color.bg : "#ffffff")};
  padding: 7px 8px !important;
  max-width: 100%;
  text-align: left;
  touch-action: manipulation;
  margin: 0;
  outline: 0;
  box-shadow: 0 0 0 0 transparent inset;
  transition: color 0.1s ease, border-color 0.1s ease;

  &:focus {
    border-color: ${p => p.theme.color.primary} !important;
  }
`;
