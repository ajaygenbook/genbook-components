import React from "react";
import styled, { css } from "styled-components";

export const Button = props => handleLinkWrapping(StyledButton, props);

const handleLinkWrapping = (Component, props) => {
  const {
    href,
    target,
    children,
    disabled,
    isLoading,
    className,
    ...rest
  } = props;

  const button = (
    <Component disabled={disabled || isLoading} className={className} {...rest}>
      {children}
    </Component>
  );

  //TODO: Needs to be tested. Might need to apply css to <a> tag
  if (href) {
    return (
      <a href={href} target={target || ""}>
        {button}
      </a>
    );
  }

  return button;
};

const StyledButton = styled.button`
  display: inline-flex;
  flex: none;
  justify-content: center;
  align-items: center;
  line-height: 17px;
  font-weight: 600;
  padding: 7px 15px;
  border-radius: 3px;
  font-size: 14px;
  color: #fff;
  cursor: pointer;
  opacity: ${props => (props.disabled ? "0.6" : "1")};
  pointer-events: ${p => p.disabled && "none"};
  background-color: ${props =>
    props.danger ? props.theme.color.danger : props.theme.color.primary};

  &:hover {
    background-color: ${props =>
      props.danger
        ? props.theme.color.dangerDark
        : props.theme.color.primaryDark};
  }
`;
