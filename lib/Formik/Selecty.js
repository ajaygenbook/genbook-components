"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Error = require("./Error");

var _Label = require("./Label");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  font-weight: 500;\n  font-size: 14px;\n  width: 100%;\n  margin-bottom: 5px;\n  position: relative;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  > div {\n    border-color: ", ";\n    border-radius: 2px;\n\n    &:focus,\n    &:hover {\n      border: 1px solid ", ";\n    }\n    & [class*=\"indicatorContainer\"] {\n      padding: 5px;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { keys.push.apply(keys, Object.getOwnPropertySymbols(object)); } if (enumerableOnly) keys = keys.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { if (i % 2) { var source = arguments[i] != null ? arguments[i] : {}; ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(arguments[i])); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(arguments[i], key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Selecty =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Selecty, _React$Component);

  function Selecty() {
    _classCallCheck(this, Selecty);

    return _possibleConstructorReturn(this, _getPrototypeOf(Selecty).apply(this, arguments));
  }

  _createClass(Selecty, [{
    key: "handleChange",
    value: function handleChange(value) {
      this.props.onChange(this.props.name, value);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          label = _this$props.label,
          name = _this$props.name,
          options = _this$props.options,
          isMulti = _this$props.isMulti,
          value = _this$props.value,
          errors = _this$props.errors,
          touched = _this$props.touched,
          className = _this$props.className,
          rest = _objectWithoutProperties(_this$props, ["id", "label", "name", "options", "isMulti", "value", "errors", "touched", "className"]);

      var customStyles = {
        control: function control(base) {
          return _objectSpread({}, base, {
            minHeight: 34
          });
        },
        dropdownIndicator: function dropdownIndicator(base) {
          return _objectSpread({}, base, {
            padding: 4
          });
        },
        clearIndicator: function clearIndicator(base) {
          return _objectSpread({}, base, {
            padding: 4
          });
        },
        multiValue: function multiValue(base) {
          return _objectSpread({}, base);
        },
        valueContainer: function valueContainer(base) {
          return _objectSpread({}, base, {
            padding: "0px 6px"
          });
        },
        input: function input(base) {
          return _objectSpread({}, base, {
            margin: 0,
            padding: 0
          });
        }
      };
      var error = touched[name] && errors[name];
      return _react["default"].createElement(StyledWrapper, {
        className: className
      }, _react["default"].createElement(_Label.Label, {
        htmlFor: id,
        label: label
      }), _react["default"].createElement(StyledSelect, _extends({
        id: id,
        name: name,
        options: options,
        isMulti: isMulti,
        onChange: this.handleChange,
        onBlur: this.handleBlur,
        value: value,
        styles: customStyles,
        error: error
      }, rest)), !!error && _react["default"].createElement(_Error.Error, {
        error: error
      }));
    }
  }]);

  return Selecty;
}(_react["default"].Component);

var _default = Selecty;
exports["default"] = _default;
var StyledSelect = (0, _styledComponents["default"])(_reactSelect["default"])(_templateObject(), function (p) {
  return p.error ? p.theme.color.danger : "#96c0e4";
}, function (p) {
  return p.theme.color.primary;
});

var StyledWrapper = _styledComponents["default"].div(_templateObject2());