"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormikInput = void 0;

var _react = _interopRequireDefault(require("react"));

var _formik = require("formik");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Label = require("./Label");

var _Error = require("./Error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  border-radius: 2px !important;\n  border: ", ";\n  background-color: ", ";\n  padding: 7px 8px !important;\n  max-width: 100%;\n  text-align: left;\n  touch-action: manipulation;\n  margin: 0;\n  outline: 0;\n  box-shadow: 0 0 0 0 transparent inset;\n  transition: color 0.1s ease, border-color 0.1s ease;\n\n  &:focus {\n    border-color: ", " !important;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  font-weight: 500;\n  font-size: 14px;\n  width: 100%;\n  margin-bottom: 5px;\n  position: relative;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var FormikInput = function FormikInput(_ref) {
  var label = _ref.label,
      id = _ref.id,
      name = _ref.name,
      className = _ref.className,
      type = _ref.type,
      rest = _objectWithoutProperties(_ref, ["label", "id", "name", "className", "type"]);

  return _react["default"].createElement(_formik.Field, _extends({
    component: InputComponent,
    label: label,
    id: id,
    name: name,
    className: className,
    type: type
  }, rest));
};

exports.FormikInput = FormikInput;

var InputComponent = function InputComponent(_ref2) {
  var _ref2$field = _ref2.field,
      name = _ref2$field.name,
      value = _ref2$field.value,
      onChange = _ref2$field.onChange,
      onBlur = _ref2$field.onBlur,
      _ref2$form = _ref2.form,
      errors = _ref2$form.errors,
      touched = _ref2$form.touched,
      id = _ref2.id,
      label = _ref2.label,
      customValue = _ref2.customValue,
      className = _ref2.className,
      type = _ref2.type,
      rest = _objectWithoutProperties(_ref2, ["field", "form", "id", "label", "customValue", "className", "type"]);

  var error = touched[name] && errors[name];
  return _react["default"].createElement(StyledWrapper, {
    className: className
  }, label && _react["default"].createElement(_Label.Label, {
    htmlFor: id,
    error: error,
    label: label
  }), _react["default"].createElement(StyledInput, _extends({
    name: name,
    id: id,
    type: type || "text",
    value: customValue || value,
    onChange: onChange,
    onBlur: onBlur,
    error: error
  }, rest)), error && _react["default"].createElement(_Error.Error, {
    error: error
  }));
};

var StyledWrapper = _styledComponents["default"].div(_templateObject());

var StyledInput = (0, _styledComponents["default"])(_formik.Field)(_templateObject2(), function (p) {
  return p.error ? "1px solid ".concat(p.theme.color.danger, " !important") : "1px solid #96c0e4 !important";
}, function (p) {
  return p.disabled ? p.theme.color.bg : "#ffffff";
}, function (p) {
  return p.theme.color.primary;
});