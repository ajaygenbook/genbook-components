"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _async = _interopRequireDefault(require("react-select/async"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  > div {\n    border-color: ", ";\n    border-radius: 2px;\n\n    &:focus,\n    &:hover {\n      border: 1px solid ", ";\n    }\n    & [class*=\"indicatorContainer\"] {\n      padding: 5px;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { keys.push.apply(keys, Object.getOwnPropertySymbols(object)); } if (enumerableOnly) keys = keys.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { if (i % 2) { var source = arguments[i] != null ? arguments[i] : {}; ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(arguments[i])); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(arguments[i], key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AsyncSelecty = function AsyncSelecty(props) {
  var customStyles = {
    control: function control(base) {
      return _objectSpread({}, base, {
        minHeight: 34
      });
    },
    dropdownIndicator: function dropdownIndicator(base) {
      return _objectSpread({}, base, {
        padding: 4
      });
    },
    clearIndicator: function clearIndicator(base) {
      return _objectSpread({}, base, {
        padding: 4
      });
    },
    multiValue: function multiValue(base) {
      return _objectSpread({}, base);
    },
    valueContainer: function valueContainer(base) {
      return _objectSpread({}, base, {
        padding: "0px 6px"
      });
    },
    input: function input(base) {
      return _objectSpread({}, base, {
        margin: 0,
        padding: 0
      });
    }
  };
  return _react["default"].createElement(StyledAsyncSelect, _extends({
    styles: customStyles
  }, props));
};

var _default = AsyncSelecty;
exports["default"] = _default;
var StyledAsyncSelect = (0, _styledComponents["default"])(_async["default"])(_templateObject(), function (p) {
  return p.error ? p.theme.color.danger : "#96c0e4";
}, function (p) {
  return p.theme.color.primary;
});