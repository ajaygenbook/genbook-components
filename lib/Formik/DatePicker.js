"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDatepicker = _interopRequireDefault(require("react-datepicker"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Label = require("./Label");

var _Error = require("./Error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  .react-datepicker__input-container {\n    display: flex;\n\n    > input {\n      text-align: left;\n      padding-left: 0.7rem;\n      background: ", ";\n      border: ", ";\n\n      &:focus {\n        border-color: ", " !important;\n      }\n    }\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  font-weight: 500;\n  font-size: 14px;\n  width: 100%;\n  margin-bottom: 5px;\n  position: relative;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var ReactDatePicker = function ReactDatePicker(_ref) {
  var disabled = _ref.disabled,
      className = _ref.className,
      touched = _ref.touched,
      errors = _ref.errors,
      name = _ref.name,
      label = _ref.label,
      rest = _objectWithoutProperties(_ref, ["disabled", "className", "touched", "errors", "name", "label"]);

  var error = touched[name] && errors[name];
  console.log("DATE ERROR", error, errors);
  return _react["default"].createElement(StyledWrapper, {
    className: className
  }, label && _react["default"].createElement(_Label.Label, {
    htmlFor: id,
    error: error,
    label: label
  }), _react["default"].createElement(StyledDate, {
    disabled: disabled,
    className: className,
    error: error
  }, _react["default"].createElement(_reactDatepicker["default"], _extends({
    disabled: disabled,
    name: name,
    popoverAtachment: "bottom left",
    popoverTargetAttachment: "top left"
  }, rest))), error && _react["default"].createElement(_Error.Error, {
    error: error
  }));
};

var _default = ReactDatePicker;
exports["default"] = _default;

var StyledWrapper = _styledComponents["default"].div(_templateObject());

var StyledDate = _styledComponents["default"].div(_templateObject2(), function (p) {
  return p.disabled ? p.theme.color.bg : "#ffffff";
}, function (p) {
  return p.error ? "1px solid ".concat(p.theme.color.danger, " !important") : "1px solid #96c0e4 !important";
}, function (p) {
  return p.theme.color.primary;
});