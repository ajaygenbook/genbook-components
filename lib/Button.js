"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Button = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: inline-flex;\n  flex: none;\n  justify-content: center;\n  align-items: center;\n  line-height: 17px;\n  font-weight: 600;\n  padding: 7px 15px;\n  border-radius: 3px;\n  font-size: 14px;\n  color: #fff;\n  cursor: pointer;\n  opacity: ", ";\n  pointer-events: ", ";\n  background-color: ", ";\n\n  &:hover {\n    background-color: ", ";\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Button = function Button(props) {
  return handleLinkWrapping(StyledButton, props);
};

exports.Button = Button;

var handleLinkWrapping = function handleLinkWrapping(Component, props) {
  var href = props.href,
      target = props.target,
      children = props.children,
      disabled = props.disabled,
      isLoading = props.isLoading,
      className = props.className,
      rest = _objectWithoutProperties(props, ["href", "target", "children", "disabled", "isLoading", "className"]);

  var button = _react["default"].createElement(Component, _extends({
    disabled: disabled || isLoading,
    className: className
  }, rest), children); //TODO: Needs to be tested. Might need to apply css to <a> tag


  if (href) {
    return _react["default"].createElement("a", {
      href: href,
      target: target || ""
    }, button);
  }

  return button;
};

var StyledButton = _styledComponents["default"].button(_templateObject(), function (props) {
  return props.disabled ? "0.6" : "1";
}, function (p) {
  return p.disabled && "none";
}, function (props) {
  return props.danger ? props.theme.color.danger : props.theme.color.primary;
}, function (props) {
  return props.danger ? props.theme.color.dangerDark : props.theme.color.primaryDark;
});