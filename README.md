# Genbook Components

This is a list of components used within the team

Feel free to fork and make contributions. We'll try to get them into the main
application.

## Setup

```
npm i genbook-components
```